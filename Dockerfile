# python:alpine is 3.{latest}
FROM python:3.9-alpine

LABEL maintainer="Carlos Carvalho"

RUN pip install requests

COPY app.py /src/

ENTRYPOINT ["python3", "/src/app.py"]
